const { Router } = require('express');
const fightService = require('../services/fightService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

// OPTIONAL TODO: Implement route controller for fights
router.get('/', (req, res) => {
  try {
    const fights = fightService.getAll();

    if (!fights) {
      res.status(404).send({
        error: true,
        message: 'Have no fights'
      });
    }
    res.send(fights);
  } catch (e) {
    res.status(500).send({
      error: true,
      message: 'Internal server error'
    });
  }
});

router.get('/:id', (req, res) => {
  try {
    const { id } = req.params;
    const foundFight = fightService.search({ id });

    if (!foundFight) {
      res.status(404).send({
        error: true,
        message: 'No fight with such id'
      });
    }
    res.send(foundFight);
  } catch (e) {
    res.status(500).send({
      error: true,
      message: 'Internal server error'
    });
  }
});

router.post('/', (req, res) => {
  const fight = req.newFight;
  try {
    const result = fightService.create(fight);

    if (!result) {
      res.status(400).send({
        error: true,
        message: 'Non validation error'
      });
    }
    res.send(result);
  } catch (e) {
    res.status(500).send({
      error: true,
      message: 'Internal server error'
    });
  }
});

router.put('/:id', (req, res) => {
  const { id } = req.params;
  const dataToUpdate = req.body;
  try {
    const updatedFight = fightService.update(id, dataToUpdate);

    if (!updatedFight) {
      res.status(404).json({
        error: true,
        message: 'No fight with such id'
      });
    }
    res.send(updatedFight);
  } catch (e) {
    res.status(500).send({
      error: true,
      message: 'Internal server error'
    });
  }
});

router.delete('/:id', (req, res) => {
  try {
    const { id } = req.params;
    const fight = fightService.delete(id);

    if (!fight) {
      res.status(404).send({
        error: true,
        message: 'Fight not found'
      });
    }
    res.send(fight);
  } catch (e) {
    res.status(500).send({
      error: true,
      message: 'Internal server error'
    });
  }
});

module.exports = router;