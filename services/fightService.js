const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
    // OPTIONAL TODO: Implement methods to work with fights
		constructor(repository) {
			this.repository = repository;
		}
	
		search(search) {
			const fights = FightRepository.repository.getOne(search);
	
			if (!fights) {
				return null;
			}
			return fights;
		}
	
		getAll() {
			const fights = FightRepository.repository.getAll();
	
			if (!fights) {
				return null;
			}
			return fights;
		}
	
		create(data) {
			const fights = FightRepository.repository.create(data);
	
			if (!fights) {
				return null;
			}
			return fights;
		}
	
		update(id, data) {
			return FightRepository.repository.update(id, data);
		}
	
		delete(id) {
			const fights = FightRepository.repository.delete(id);
	
			if (!fights.length) {
				return null;
			}
			return fights[0];
		}
}

module.exports = new FightersService(FightRepository);